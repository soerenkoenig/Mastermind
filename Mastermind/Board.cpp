﻿#include "Board.h"

namespace mastermind
{
	std::ostream& operator<<(std::ostream& out, const GameState& gameState)
	{
		switch (gameState)
		{
		case GameState::eSecretSelection:
			out << "Secret Selection";
			break;

		case GameState::eGuessing:
			out << "Guessing";
			break;
		case GameState::eGameOver:
			out << "Game over";
			break;
		}
		return out;
	}

	Board::Board(const std::size_t& maxGuessCount) : mState(GameState::eSecretSelection),
		mMaxGuessCount(maxGuessCount)
	{
		if (maxGuessCount < 1)
		{
			throw std::invalid_argument("Error: maxGuessCount < 1 is not allowed.");
		}
	}

	void Board::startNewGame()
	{
		mGuesses.clear();
		mState = GameState::eSecretSelection;
		notify(*this);
	}

	void Board::setSecret(const Code& secret)
	{
		if (mState != GameState::eSecretSelection)
		{
			throw std::runtime_error("Error: Selecting secret is not allowed in this state");
		}

		mSecret = secret;
		mState = GameState::eGuessing;
		notify(*this);
	}

	void Board::makeGuess(const Code& code)
	{
		if (mState != GameState::eGuessing)
		{
			throw std::runtime_error("Error: make guess is not allowed in this state");
		}

		auto score = computeScore(code);
		mGuesses.emplace_back(code, score);
		if (mGuesses.size() == mMaxGuessCount || score.mBlackCount == code.size())
		{
			mState = GameState::eGameOver;
		}

		notify(*this);
	}

	const std::vector<Guess>& Board::guesses() const
	{
		return mGuesses;
	}

	bool Board::isWon() const
	{
		if (mState != GameState::eGameOver)
		{
			return false;
		}

		const auto& lastGuess = mGuesses.back();
		return lastGuess.mScore.mBlackCount == lastGuess.mCode.size();
	}

	bool Board::isLost() const
	{
		if (mState != GameState::eGameOver)
		{
			return false;
		}

		const Guess& lastGuess = mGuesses.back();
		return lastGuess.mScore.mBlackCount < lastGuess.mCode.size();
	}

	bool Board::isGameOver() const
	{
		return mState == GameState::eGameOver;
	}

	const GameState& Board::state() const
	{
		return mState;
	}

	const Code& Board::secret() const
	{
		if (!isGameOver())
		{
			throw std::runtime_error("Error: You are cheating! Do not query secret code if game is not over!R");
		}
		return mSecret;
	}

	Score Board::computeScore(Code code, Code secret)
	{
		Score score{ 0, 0 };
		for (std::size_t i = 0; i < code.size(); ++i)
		{
			if (code[i] == secret[i])
			{
				++score.mBlackCount;
				code[i] = CodePeg::eInvalid;
				secret[i] = CodePeg::eInvalid;
			}
		}

		for (std::size_t i = 0; i < code.size(); ++i)
		{
			if (code[i] == CodePeg::eInvalid)
				continue;

			for (std::size_t j = 0; j < code.size(); ++j)
			{
				if (i == j)
					continue;

				if (code[i] == secret[j])
				{
					++score.mWhiteCount;
					secret[j] = CodePeg::eInvalid;
					break;
				}
			}
		}
		return score;
	}

	Score Board::computeScore(const Code& code) const
	{
		return computeScore(code, mSecret);
	}

	std::ostream& operator<<(std::ostream& out, const Board& board)
	{
		std::size_t i = 0;
		std::cout << "Mastermind Board\n";
		std::cout << "================\n\n";
		std::cout << "Phase: " << board.state() << std::endl;

		for (auto&& guess : board.guesses())
		{
			out << ++i << ":" << guess << std::endl;
		}

		if (board.isGameOver())
		{
			if (board.isWon())
			{
				std::cout << "Code was broken with " << board.guesses().size() << " guesses" << std::endl;
			}
			else
			{
				std::cout << "Code was not broken. The correct code was: " << board.secret() << std::endl;
			}
		}

		return out;
	}
}
