﻿#include "ScopeGuard.h"

namespace mastermind
{
	ScopeGuard& ScopeGuard::operator=(ScopeGuard&& other) noexcept
	{
		if (this != &other)
		{
			mOnDestroyCallback = std::move(other.mOnDestroyCallback);
		}
		return *this;
	}

	ScopeGuard::ScopeGuard(std::function<void()> onDestroyCallBack) : mOnDestroyCallback(
		std::move(onDestroyCallBack))
	{
	}

	void ScopeGuard::reset(std::function<void()> onDestroyCallBack)
	{
		mOnDestroyCallback = std::move(onDestroyCallBack);
	}

	ScopeGuard::ScopeGuard(ScopeGuard&& other) noexcept
	{
		*this = std::move(other);
	}

	ScopeGuard::~ScopeGuard()
	{
		if (mOnDestroyCallback)
			mOnDestroyCallback();
	}
}
