﻿#include "Code.h"
#include <cmath>
#include <sstream>

namespace mastermind
{
	Code::Code() : Code(CodePeg::eInvalid, CodePeg::eInvalid, CodePeg::eInvalid, CodePeg::eInvalid)
	{
	}

	Code::Code(const CodePeg& c0, const CodePeg& c1, const CodePeg& c2, const CodePeg& c3): mCodePegs{c0, c1, c2, c3}
	{
	}

	Code::iterator Code::begin()
	{
		return mCodePegs.begin();
	}

	Code::iterator Code::end()
	{
		return mCodePegs.end();
	}

	Code::const_iterator Code::cbegin() const
	{
		return mCodePegs.cbegin();
	}

	Code::const_iterator Code::cend() const
	{
		return mCodePegs.cend();
	}

	Code::const_iterator Code::begin() const
	{
		return mCodePegs.begin();
	}

	Code::const_iterator Code::end() const
	{
		return mCodePegs.end();
	}

	CodePeg& Code::operator[](const size_type& index)
	{
		return mCodePegs[index];
	}

	const CodePeg& Code::operator[](const size_type& index) const
	{
		return mCodePegs[index];
	}

	Code::size_type Code::size() const noexcept
	{
		return mCodePegs.size();
	}

	Code& Code::operator++()
	{
		auto it = mCodePegs.rbegin();
		while (it != mCodePegs.rend())
		{
			if (*it != CodePeg::eLastColor)
			{
				*it = static_cast<CodePeg>(static_cast<int>(*it) + 1);
				break;
			}
			else
			{
				*it = CodePeg::eFirstColor;
				++it;
			}
		}
		return *this;
	}

	Code Code::operator++(int)
	{
		const auto temp = *this;
		operator++();
		return temp;
	}

	std::ostream& operator<<(std::ostream& out, const Code& code)
	{
		for (auto&& codePeg : code)
		{
			out << codePeg;
		}
		return out;
	}

	std::istream& operator>>(std::istream& in, Code& code)
	{
		for (auto&& codePeg : code)
		{
			in >> codePeg;
			if (!in)
				return in;
		}
		return in;
	}


	Code readCodeFromConsole()
	{
		Code code;
		std::cout << "Enter code [length must be equal to " << code.size() <<
			". You can use the following chars: RGBCMY]:" << std::endl;

		while (true)
		{
			std::string input;
			std::cin >> input;
			if (input.size() == code.size())
			{
				std::stringstream ss(input);
				if (ss >> code)
				{
					std::cout << std::endl;
					break;
				}
			}
			std::random_device r;
			std::default_random_engine eng(r());
			std::cout << "Code was invalid please try again e.g. [" << randomCode(eng) << "]: " << std::endl;
		}

		return code;
	}

	std::vector<Code> generateAllPossibleCodes()
	{
		Code c;
		std::fill(c.begin(), c.end(), CodePeg::eFirstColor);

		const auto numColors = static_cast<int>(CodePeg::eLastColor) + 1;
		const auto n = static_cast<std::size_t>(std::pow(numColors, c.size()));
		std::vector<Code> allCodes(n);
		for (std::size_t i = 0; i < n; ++i)
		{
			allCodes[i] = c++;
		}
		return allCodes;
	}
}
