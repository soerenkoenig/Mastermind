#include "Game.h"
#include "KnuthCodeBreaker.h"


int main()
{
	using namespace mastermind;

	std::cout << "Welcome to Mastermind!\n";
	std::cout << "======================\n" << std::endl;

	Game game;
	game.registerCodeBreaker<KnuthCodeBreaker>("Knuth");

	game.play();
}
