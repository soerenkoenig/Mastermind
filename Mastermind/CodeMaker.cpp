﻿#include "CodeMaker.h"

namespace mastermind
{
	RandomCodeMaker::RandomCodeMaker() :
		mRandomEngine(std::random_device()())
	{
	}

	void RandomCodeMaker::selectSecret(Board& board)
	{
		board.setSecret(randomCode(mRandomEngine));
	}


	void HumanCodeMaker::selectSecret(Board& board)
	{
		board.setSecret(readCodeFromConsole());
	}
}
