﻿#pragma once

#include "ScopeGuard.h"

#include <vector>
#include <functional>

namespace mastermind
{
	/// <summary>
	/// An Observable keeps a vector of registered callback functors with the following signature void func(Args... args).
	/// The notify(Args... args) method than calls all registered callbacks with provided arguments Args
	/// </summary>
	/// <typeparam name="Args">argument list forwarded by notify to registered callbacks</typeparam>
	template <class... Args>
	class Observable
	{
	public:

		/// <summary>
		/// Register a callback.
		/// </summary>
		/// <typeparam name="Callback">callback functor type with given signature: void func(Args... args)</typeparam>
		/// <param name="cb">call back to be registered</param>
		/// <returns>a scope guard is returned which calls unregisterCallback on destruction</returns>
		template <typename Callback>
		ScopeGuard registerCallback(Callback&& cb)
		{
			mCallbacks.emplace_back(std::forward<Callback>(cb));

			return ScopeGuard([this, handle = mCallbacks.size() - 1]()
			{
				unregisterCallback(handle);
			});
		}

		/// <summary>
		/// Notify (call) all registered callbacks with given argument list.
		/// </summary>
		/// <param name="args"> arguments to call callbacks with</param>
		void notify(Args ... args)
		{
			for (auto&& callback : mCallbacks)
			{
				callback(args...);
			}
		}

	private:
		/// <summary>
		/// Un-register callbacks by index.
		/// </summary>
		/// <param name="index">index of callback to un-register</param>
		void unregisterCallback(std::size_t index)
		{
			mCallbacks.erase(mCallbacks.begin() + index);
		}

		/// <summary>
		/// registered callbacks
		/// </summary>
		std::vector<std::function<void(Args ...)>> mCallbacks;
	};
}
