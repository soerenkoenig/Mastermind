#pragma once

#include <iostream>

namespace mastermind
{
	/// <summary>
	/// A CodePeg of a given color (digit or char of a Code).
	/// </summary>
	enum class CodePeg : int
	{
		eInvalid = -1,
		eRed = 0,
		eGreen,
		eBlue,
		eCyan,
		eMagenta,
		eYellow,
		eFirstColor = eRed,
		eLastColor = eYellow,
	};

	std::ostream& operator<<(std::ostream& out, const CodePeg& codePeg);

	std::istream& operator>>(std::istream& in, CodePeg& codePeg);
}

