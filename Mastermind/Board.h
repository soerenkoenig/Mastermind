﻿#pragma once

#include "Observable.h"
#include "Guess.h"

#include <vector>

namespace mastermind
{
	/// <summary>
	/// Game state -> eSecretSelection -> eGuessing -> eGameOver
	/// </summary>
	enum class GameState
	{
		/// <summary>
		/// game state in which secret selection must be done by code maker
		/// </summary>
		eSecretSelection,

		/// <summary>
		/// game state in which code breaker must make guesses 
		/// </summary>
		eGuessing,

		/// <summary>
		/// game over state reached when max number of guesses is reached or if code was broken by code breaker
		/// </summary>
		eGameOver,
	};

	std::ostream& operator<<(std::ostream& out, const GameState& gameState);

	/// <summary>
	/// The Board is the data model which stores the game state, the secret code and all made code guesses with its associated scores.
	/// </summary>
	class Board : public Observable<const Board&>
	{
	public:

		/// <summary>
		/// initializes board and sets max number of allowed guesses
		/// </summary>
		/// <param name="maxGuessCount">maximum number of guesses for code breaker</param>
		explicit Board(const std::size_t& maxGuessCount = 10);

		/// <summary>
		/// reset game state to SecretSelection and clears all made guesses
		/// </summary>
		void startNewGame();

		/// <summary>
		/// set secret to be called by Code Maker in SecretSelection State
		/// </summary>
		/// <param name="secret">secret code to set</param>
		void setSecret(const Code& secret);

		/// <summary>
		/// make guess to be called by Code Breaker in Guessing State
		/// </summary>
		/// <param name="code"></param>
		void makeGuess(const Code& code);

		/// <summary>
		/// get all made guesses
		/// </summary>
		/// <returns></returns>
		const std::vector<Guess>& guesses() const;

		/// <summary>
		/// checks if game is won by CodeBreaker
		/// </summary>
		/// <returns>true if code was broken false otherwise</returns>
		bool isWon() const;

		/// <summary>
		/// checks if game is lost by CodeBreaker
		/// </summary>
		/// <returns>true if game is over and code was not broken</returns>
		bool isLost() const;

		/// <summary>
		/// checks if game is over which means code was broken or max num guesses is reached
		/// </summary>
		/// <returns>true if game is over, false otherwise</returns>
		bool isGameOver() const;

		/// <summary>
		/// get current game state
		/// </summary>
		/// <returns>current game state</returns>
		const GameState& state() const;

		/// <summary>
		/// get secret code, only allowed to be called if game is over (otherwise exception is thrown)
		/// </summary>
		/// <returns>secret code</returns>
		const Code& secret() const;

		/// <summary>
		/// compute score for given code (guess) and secret
		/// </summary>
		/// <param name="code">guessed code</param>
		/// <param name="secret">secret code to compare with</param>
		/// <returns>computed score</returns>
		static Score computeScore(Code code, Code secret);

	private:
		/// <summary>
		/// calls compute score for given code (guess) and internally stored secret
		/// </summary>
		/// <param name="code"></param>
		/// <returns></returns>
		Score computeScore(const Code& code) const;

		/// <summary>
		/// all made guesses inserted via makeGuess (code with scores)
		/// </summary>
		std::vector<Guess> mGuesses;

		/// <summary>
		/// secret code set via setSecret
		/// </summary>
		Code mSecret;

		/// <summary>
		/// current game state
		/// </summary>
		GameState mState;

		/// <summary>
		/// max number of guesses used to decide if game is over
		/// </summary>
		std::size_t mMaxGuessCount;
	};

	std::ostream& operator<<(std::ostream& out, const Board& board);
}
