﻿#pragma once
#include <memory>
#include <functional>
#include <map>
#include <algorithm>
#include <vector>

namespace mastermind
{
	/// <summary>
	/// A factory class.  Default constructable types (ConcreteProduct) derived from type Product can be registered under a unique key.
	/// Then all registered types can be created by key using the create method.
	/// </summary>
	/// <typeparam name="Product">base class for all product which can be registered/created</typeparam>
	/// <typeparam name="Key">key used by create method to select product</typeparam>
	template <typename Product, typename Key>
	class Factory
	{
	public:
		/// <summary>
		/// create function type
		/// </summary>
		using CreateFunc = std::function<std::unique_ptr<Product>()>;

		/// <summary>
		/// Register a create method under given key.
		/// </summary>
		/// <param name="key">key identifier to register product</param>
		/// <param name="func"> create method functor with signature: std::unique_ptr<Product> func(void)</param>
		/// <returns>true if registered successfully, false if key is already in use</returns>
		bool registerCreateMethod(const Key& key, CreateFunc func)
		{
			auto it = mCreateMethods.find(key);
			if (it != mCreateMethods.end())
				return false;

			mCreateMethods[key] = func;
			return true;
		}

		/// <summary>
		/// Register a create method under given key which default constructs given ConcreteProduct
		/// </summary>
		/// <typeparam name="ConcreteProduct"> product to register, needs to be derived by Product</typeparam>
		/// <param name="key">key identifier to register product</param>
		/// <returns>true if registered successfully, false if key is already in use</returns>
		template <typename ConcreteProduct>
		bool registerType(const Key& key)
		{
			static_assert(std::is_base_of_v<Product, ConcreteProduct>,
				"Error: ConcreteProduct is not derived from Product.");

			return registerCreateMethod(key, []()-> std::unique_ptr<Product>
			                            {
				                            return std::make_unique<ConcreteProduct>();
			                            }
			);
		}

		/// <summary>
		/// Create product by given key.
		/// </summary>
		/// <param name="key">key of concrete product to create</param>
		/// <returns>registered product or empty unique pointer if key was not found</returns>
		std::unique_ptr<Product> create(const Key& key) const
		{
			auto it = mCreateMethods.find(key);
			if (it != mCreateMethods.end())
				return it->second();

			return {};
		}

		/// <summary>
		/// Get vector of all keys of registered products.
		/// </summary>
		/// <returns>all keys</returns>
		std::vector<Key> keys() const
		{
			std::vector<Key> keys(mCreateMethods.size());
			std::transform(mCreateMethods.begin(), mCreateMethods.end(), keys.begin(),
			               [](auto&& keyValuePair) { return keyValuePair.first; });
			return keys;
		}

	private:
		/// <summary>
		/// create methods stored by key
		/// </summary>
		std::map<Key, CreateFunc> mCreateMethods;
	};
}
