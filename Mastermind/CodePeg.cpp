﻿#include "CodePeg.h"

namespace mastermind
{
	std::ostream& operator<<(std::ostream& out, const CodePeg& codePeg)
	{
		switch (codePeg)
		{
		case CodePeg::eRed: out << 'R';
			break;
		case CodePeg::eGreen: out << 'G';
			break;
		case CodePeg::eBlue: out << 'B';
			break;
		case CodePeg::eCyan: out << 'C';
			break;
		case CodePeg::eMagenta: out << 'M';
			break;
		case CodePeg::eYellow: out << 'Y';
			break;
		case CodePeg::eInvalid:
			break;
		}
		return out;
	}

	std::istream& operator>>(std::istream& in, CodePeg& codePeg)
	{
		switch (in.get())
		{
		case 'r':
		case 'R': codePeg = CodePeg::eRed;
			break;
		case 'g':
		case 'G': codePeg = CodePeg::eGreen;
			break;
		case 'b':
		case 'B': codePeg = CodePeg::eBlue;
			break;
		case 'c':
		case 'C': codePeg = CodePeg::eCyan;
			break;
		case 'm':
		case 'M': codePeg = CodePeg::eMagenta;
			break;
		case 'y':
		case 'Y': codePeg = CodePeg::eYellow;
			break;

		default:
			in.setstate(std::ios_base::failbit);
		}
		return in;
	}
}
