﻿#include "Guess.h"

namespace mastermind
{
	Guess::Guess(const Code& code, const Score& score): mCode(code), mScore(score)
	{
	}

	std::ostream& operator<<(std::ostream& out, const Guess& guess)
	{
		return out << guess.mCode << " : " << guess.mScore;
	}
}
