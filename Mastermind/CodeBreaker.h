﻿#pragma once

#include "Board.h"

namespace mastermind
{
	/// <summary>
	/// Code Breaker interface. The codebreaker tries to identify the secret code. By making guesses.
	/// </summary>
	class ICodeBreaker
	{
	public:
		ICodeBreaker() = default;
		virtual ~ICodeBreaker() = default;
		ICodeBreaker& operator=(const ICodeBreaker& other) = delete;
		ICodeBreaker& operator=(ICodeBreaker&& other) = delete;

		/// <summary>
		/// Must be implemented by concrete code breaker.
		/// The code breaker can inspect all guesses (codes with scores) and must call board.makeGuess(newCodeGuess) to add a new guess to the board.
		/// </summary>
		/// <param name="board">board to inspect old guesses and to assign new code guess</param>
		virtual void makeGuess(Board& board) = 0;

	protected:
		ICodeBreaker(const ICodeBreaker& other);
		ICodeBreaker(ICodeBreaker&& other) noexcept;
	};

	/// <summary>
	/// A human code breaker which ask the user via console input for the next guess.
	/// </summary>
	class HumanCodeBreaker final : public ICodeBreaker
	{
	public:
		/// <summary>
		/// Gets next code guess from user via std::cin and assign it to the board 
		/// </summary>
		/// <param name="board">board to inspect old guesses and to assign new code guess</param>
		void makeGuess(Board& board) override;
	};

	/// <summary>
	/// A random code breaker which tries to break the secret code by stupid random guesses
	/// </summary>
	class RandomCodeBreaker final : public ICodeBreaker
	{
	public:
		RandomCodeBreaker();

		/// <summary>
		/// Assigns random guesses to the board.
		/// </summary>
		/// <param name="board">board to inspect old guesses and to assign new code guess</param>
		void makeGuess(Board& board) override;
	private:
		std::default_random_engine mRandomEngine;
	};
}
