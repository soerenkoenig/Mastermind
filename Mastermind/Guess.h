﻿#pragma once

#include "Code.h"
#include "Score.h"

namespace mastermind
{
	/// <summary>
	/// A guess simply stores a Code with its associated Score. Guesses are created and stored by the Board which keeps the complete state of a game.
	/// </summary>
	struct Guess
	{
		Guess(const Code& code, const Score& score);
		/// <summary>
		/// the guessed code word
		/// </summary>
		Code mCode;

		/// <summary>
		/// the associated score
		/// </summary>
		Score mScore;
	};

	std::ostream& operator<<(std::ostream& out, const Guess& guess);
}
