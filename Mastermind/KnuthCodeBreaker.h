﻿#pragma once

#include "CodeBreaker.h"

namespace mastermind
{
	/// <summary>
	/// Implementation of Donald E. Knuth's algorithm to solve Mastermind.
	/// See https://www.cs.uni.edu/~wallingf/teaching/cs3530/resources/knuth-mastermind.pdf
	/// </summary>
	class KnuthCodeBreaker final : public ICodeBreaker
	{
	public:
		KnuthCodeBreaker();

		void makeGuess(Board& board) override;

	private:

		void pruneCandidates(const Code& code, const Score& score);

		Code minMaxSearch();

		Code mCurrentGuess;
		std::vector<Code> mCombinations;
		std::vector<Code> mCandidates;
	};
}
