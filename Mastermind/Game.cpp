﻿#include "Game.h"
#include <sstream>

namespace mastermind
{
	std::size_t readSelectionFromConsole(const std::size_t& minValue, const std::size_t& maxValue)
	{
		std::cout << "Enter Selection [" << minValue << ", " << maxValue << "]: ";
		while (true)
		{
			std::string input;
			std::cin >> input;
			std::stringstream ss(input);
			std::size_t selection;
			if (ss >> selection && minValue <= selection && selection <= maxValue)
			{
				std::cout << std::endl;
				return selection;
			}
			std::cout << "Selection was invalid. Please enter a valid selection [" << minValue << ", " << maxValue <<
				"]: ";
		}
	}

	Game::Game()
	{
		mBoardView = mBoard.registerCallback([&](const Board&)
		{
			std::cout << mBoard << std::endl;
		});

		mCodeMakerFactory.registerType<RandomCodeMaker>("Random");
		mCodeMakerFactory.registerType<HumanCodeMaker>("Human");

		mCodeBreakerFactory.registerType<HumanCodeBreaker>("Human");
		mCodeBreakerFactory.registerType<RandomCodeBreaker>("Random");
	}

	void Game::selectCodeMaker()
	{
		std::cout << "Select Code Maker:\n";
		auto keys = mCodeMakerFactory.keys();
		for (std::size_t i{}; i < keys.size(); ++i)
		{
			std::cout << i << " " << keys[i] << std::endl;
		}
		const auto selection = readSelectionFromConsole(0, keys.size() - 1);
		mCodeMaker = mCodeMakerFactory.create(keys[selection]);
	}

	void Game::selectCodeBreaker()
	{
		std::cout << "Select Code Breaker:\n";

		auto keys = mCodeBreakerFactory.keys();
		for (std::size_t i{}; i < keys.size(); ++i)
		{
			std::cout << i << " " << keys[i] << std::endl;
		}
		const auto selection = readSelectionFromConsole(0, keys.size() - 1);
		mCodeBreaker = mCodeBreakerFactory.create(keys[selection]);
	}

	void Game::play()
	{
		while (true)
		{
			selectCodeMaker();
			selectCodeBreaker();

			mBoard.startNewGame();

			mCodeMaker->selectSecret(mBoard);

			while (!mBoard.isGameOver())
			{
				mCodeBreaker->makeGuess(mBoard);
			}

			std::cout << "Wanna play again? [y/n]: ";

			char answer;
			std::cin >> answer;
			if (answer != 'y' && answer != 'Y')
			{
				break;
			}
		}
	}

	std::size_t Game::guessCount() const
	{
		return mBoard.guesses().size();
	}

	bool Game::isWon() const
	{
		return mBoard.isWon();
	}
}
